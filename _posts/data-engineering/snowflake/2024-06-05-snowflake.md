---
title: Snowflake
date: 2024-06-05 11:59:00 +0100
categories: [data,snowflake]
tags: [data,snowflake]     # TAG names should always be lowercase
pin: true
---

## usefull snowflake commands

### create zero-copy clone of schema

```sql
CREATE SCHEMA new_schema_name CLONE existing_schema_name;
```

Replace `new_schema_name` with the name of the new schema you want to create and `existing_schema_name` with the name of the existing schema you want to clone.

For example, to create a clone of a schema named `original_schema` and call it `cloned_schema`, you would use:

```sql
CREATE SCHEMA cloned_schema CLONE original_schema;
```

This command will create a new schema `cloned_schema` which is a zero-copy clone of `original_schema`.

### Change users password

```sql
USE ROLE USERADMIN;
ALTER USER john_doe SET PASSWORD = 'new_secure_password';
```

### set ssh-key as authentication for user

To generate an encrypted version, use the following:

```bash
cd .ssh && openssl genrsa 2048 | openssl pkcs8 -topk8 -v2 des3 -inform PEM -out snow_devusr_rsa_key.p8
```

Generate a public key for the private encrypted key:

```bash
openssl rsa -in snow_devusr_rsa_key.p8 -pubout -out snow_devusr_rsa_key.pub
```

Assign the public key to a Snowflake user:

```sql
USE ROLE ACCOUNTADMIN;
ALTER USER jsmith SET RSA_PUBLIC_KEY='MIIBIjANBgkqh...';
```
