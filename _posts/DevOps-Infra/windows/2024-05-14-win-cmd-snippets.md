---
title: Windows cmd
date: 2024-05-14 10:59:00 +0100
categories: [windows,cmd]
tags: [windows,cmd]     # TAG names should always be lowercase
pin: false
---

### Links

- [Win useful cmd](#win-useful-cmd)
  - [Working with SSH-keys](#working-with-ssh-keys)
  - [Useful cmd in PowerShell](#useful-cmd-in-powershell)
  - [Working with Files \& Folders](#working-with-files--folders)
  - [Extras cmd](#extras-cmd)

Useful links👇

- [Using ssh-key based auth on Win](https://woshub.com/using-ssh-key-based-authentication-on-windows/)

## Windows server RD create certificates command

```powershell
New-SelfSignedCertificate -certstorelocation cert:\localmachine\my -dnsname "RDP"
```

```powershell
$pwd = ConvertTo-SecureString -String "P@ssw0rd" -Force -AsPlainText
```

## Windows SSH setup

1. Install `openSSH Server` and `openSSH Client` from `Settings -> Apps -> Optional Features`

2. Open-SSH Setup in `powershell` as administrator

```powershell
$PSVersionTable.PSVersion
```

```powershell
(New-Object Security.Principal.WindowsPrincipal([Security.Principal.WindowsIdentity]::GetCurrent())).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)
```

need to get `True`

- Install OpenSSH Client

```powershell
Add-WindowsCapability -Online -Name OpenSSH.Client~~~~0.0.1.0
```

- Install OpenSSH Server

```powershell
Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0
```

- Start the sshd service

```powershell
Start-Service sshd
```

```powershell
Restart-service sshd
```

- Auto startup

```powershell
Set-Service -Name sshd -StartupType 'Automatic'
```

- Confirm the Firewall rule is configured. It should be created automatically by setup. Run the following to verify

```powershell
if (!(Get-NetFirewallRule -Name "OpenSSH-Server-In-TCP" -ErrorAction SilentlyContinue | Select-Object Name, Enabled)) {
    Write-Output "Firewall Rule 'OpenSSH-Server-In-TCP' does not exist, creating it..."
    New-NetFirewallRule -Name 'OpenSSH-Server-In-TCP' -DisplayName 'OpenSSH Server (sshd)' -Enabled True -Direction Inbound -Protocol TCP -Action Allow -LocalPort 22
} else {
    Write-Output "Firewall rule 'OpenSSH-Server-In-TCP' has been created and exists."
}
```

### connect to OpenSSH Server from a Windows or Windows Server device with the OpenSSH client installed

```powershell
ssh domain\username@servername
```

```powershell
ssh robsdomain\paul@<ip-address>
```

```powershell
# Set the sshd service to be started automatically
Get-Service -Name sshd | Set-Service -StartupType Automatic

# Now start the sshd service
Start-Service sshd
```

generate new key

```powershell
ssh-keygen -t ed25519
``````

```powershell
# By default the ssh-agent service is disabled. Configure it to start automatically.
# Make sure you're running as an Administrator.
Get-Service ssh-agent | Set-Service -StartupType Automatic

# Start the service
Start-Service ssh-agent

# This should return a status of Running
Get-Service ssh-agent

# Now load your key files into ssh-agent
ssh-add $env:USERPROFILE\.ssh\id_ed25519
```

- On Windows, the ssh-copy-id command is not available, use this:

```powershell
cat ~/.ssh/id_ed25519.pub | ssh paul@192.168.1.228 "cat >> ~/.ssh/authorized_keys"
```

ssh using that key

```powershell
ssh -i ~/.ssh/id_ed25519 paul@192.168.1.169
# OR if naming is standart
ssh paul@192.168.1.169
```

IF port is different:

```powershell
cat ~/.ssh/id_ed25519.pub | ssh -p 23 paul@80.2.242.41 "cat >> ~/.ssh/authorized_keys"
```

```powershell
ssh -i ~/.ssh/id_ed25519 -p 23 paul@192.168.1.169
# OR if naming is standart
ssh -p 23 paul@192.168.1.169
```

## Win useful cmd

### Working with SSH-keys

```powershell
ssh-add "C:\Users\Administrator\.ssh\id_ed25519"
```

- add to cey to `authorized_keys`

```powershell
scp C:\Users\USERNAME\.ssh\id_ed25519.pub admin@192.168.0.5:c:\Users\Admininstrator\.ssh\authorized_keys
```

- SSH to somewhere

```powershell
ssh domain\username@server-name
```

- SSH to remote win server as Admin

```powershell
ssh domain\Administrator@server-name
```

- SSH to remote win server as Admin using local SSH key

```powershell
ssh Administrator@win-remote-ip-address -i "C:\Users\your-user\.ssh\id_ed25519"
```

- Useful cmd in PShell (Find Out and Fix!)

- Copy Other commands (find out exactly what it does!)

```powershell
Copy-Item (Join-Path $PSScriptRoot MoveIt1.txt) (Join-Path $env:USERPROFILE "\AppData\Roaming\") -Force
Copy-Item (Join-Path $PSScriptRoot MoveIt2.txt) "C:\Program Files (x86)\" -Force
Copy-Item -Path "C:\temp\files\*"  -Destination "d:\temp\"
```

### Useful cmd in PowerShell

- Start notepad

```powershell
Start-Process notepad
```

- Restart-Computer

```powershell
shutdown /s
```

- Find out this one!

```powershell
Get-ChildItem -Path "C:\Program Files\Folder_Name" -Recurse | Select FullName
```

- Find out this ones too

```powershell
get-disk
get-volume
```

### How to Enable or Disable Copy/Paste via RDP Clipboard on Windows?

- Run the Local Group Policy Editor: `gpedit.msc`
- Go to `Computer Configuration -> Administrative Templates -> Windows Components -> Remote Desktop Services -> Remote Desktop Session Host -> Device and Resource Redirection`
- set the following policies to Enabled:

```text
Do not allow Clipboard redirection
Do not allow drive redirection
```

### Working with Files & Folders

- get list of folder content

```powershell
Get-ChildItem "C:\"
```

- get list of `Program Files`

```powershell
Get-ChildItem -Path "C:\Program Files"
```

- Set working dir

```powershell
Set-Location "C:\Users\username\Documents"
```

- Copy All folder content ???

```powershell
Copy-Item "E:\Folder1" -Destination "E:\Folder2" -Recurse
```

- Move folder

```powershell
Move-Item -Path "E:\Folder1" -Destination "E:\Folder2"
```

- Copy file

```powershell
Copy-Item -Path "C:\temp\files\la-ams-ad01-log-1.txt" -Destination "d:\temp\"
```

- Copy file. `Copy` is a shorthand for Copy-Item

```powershell
Copy "C:\temp\files\la-ams-ad01-log-1.txt" "d:\temp\"
```

- Remove file

```powershell
Remove-Item E:\Folder1\Test.txt
```

- Remove folder with files

```powershell
Remove-Item -Path <path to the directory> -Force -Recurse
```

- Get file content

```powershell
Get-Content "E:\Folder1\Test.txt"
```

### Extras cmd

- Get and Set ExecutionPolicy

```powershell
Get-ExecutionPolicy
Set-ExecutionPolicy RemoteSigned
```

- get windows PC name

```powershell
Get-Service -Name "Win*"
```

- run clipboard

```powershell
rdpclip.exe
```

- CHECK..

```powershell
Get-Service ssh-agent | Set-Service -StartupType Automatic -PassThru | Start-Service
```

```powershell
git config --global core.sshCommand C:/Windows/System32/OpenSSH/ssh.exe
```

- Match Group administrators

```powershell
#       AuthorizedKeysFile __PROGRAMDATA__/ssh/administrators_authorized_keys
AllowGroups users
```

## Thinkpad Windows Re-Install

1. turn PC on, insert AC charger
2. remove AC charger and battery
3. press power button for 30 sec
4. connect AC charger without battery
5. turn on PC, boot to OS
6. hold power button, do NOT shutdown from windows
7. turn on laptop, press ENTER key and then F1 to enter BIOS
8. F9 to load default settings. F10 exit saving changes
10. restart and enter BIOS SETUP
11. F10 exit saving changes
12. Install windows. Insert usb. Shutdown windows, turn on windows, while booting enter boot menu
