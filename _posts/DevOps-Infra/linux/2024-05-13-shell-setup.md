---
title: Shell Setup
date: 2024-05-13 16:55:00 +0100
categories: [shell,setup]
tags: [shell,setup]     # TAG names should always be lowercase
---

## shell setup

[Full Instructions](https://www.howtogeek.com/258518/how-to-use-zsh-or-another-shell-in-windows-10/)

Install `zsh`:

```bash
sudo apt-get install -y zsh
```

set zsh to start automaticaly on bash and add `home` alias:

```bash
# add this after `# for examples`
# Launch Zsh auto
if [ -t 1 ]; then
exec zsh
fi
```

```bash
# add this
alias home='cd /home/paul/'
```

```bash
source ~/.bashrc
```

### Install `PowerLevel10k`

```bash
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/powerlevel10k
echo 'source ~/powerlevel10k/powerlevel10k.zsh-theme' >>~/.zshrc
```

```bash
p10k configure
```

### Install `PowerLevel10k` (if you have `Oh My Zsh!` already) - from old files

```bash
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
```

### Install `Oh My Zsh!` only

```bash
sudo apt-get install curl git zsh
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

### Usefull Plugins

- zsh-autosuggestions and zsh-syntax-highlighting
autosuggestions

```bash
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
```

syntax-highlighting

```bash
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
```

add them to `.zshrc`

```bash
plugins=(git sudo aliases python virtualenv last-working-dir zsh-syntax-highlighting zsh-autosuggestions)
# extras: systemd 1password sudo git-auto-fetch gcloud ssh-agent
```

## Paul, check and compare

- ZSH

```bash
sudo apt install zsh
zsh --version
$ echo $SHELL
$ chsh -s $(which zsh)
# or
$ chsh -s /usr/bin/zsh
$ sudo apt --purge remove zsh
$ chsh -s $(which "SHELL NAME")
```

- powerlevel10k

```bash
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/powerlevel10k
echo 'source ~/powerlevel10k/powerlevel10k.zsh-theme' >>~/.zshrc
```
